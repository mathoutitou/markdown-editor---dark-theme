import React, { Component, Fragment } from 'react'
import './App.css'
import marked from 'marked'

import { sampleText } from './sampleText'

class App extends Component {
  // Apparait dans l'inspecteur -> Components (React Dev Tool)
  state = {
    text: sampleText
  }

  /** Mise en place du localStorage : garder la valeur saisie en mémoire
   * pour ne pas la perdre même si on actualise la page**/

  componentDidMount () {
    console.log('Moi petit composant, je suis monté')
    const text = localStorage.getItem('text')

    // si il n'y a pas de texte, met moi le sampleText
    if (text) {
      this.setState({ text })
    } else {
      this.setState({ text: sampleText })
    }
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    console.log('ma valeur est updaté')
    const { text } = this.state
    localStorage.setItem('text', text)
  }

  handleChange = event => {
    const text = event.target.value
    this.setState({ text })
  }

  // Empêche de mettre du HTML dans le textarea pour qu'il soit converti
  renderText = text => {
    const __html = marked(text, { sanitize: true })
    return { __html }
  }

  render () {
    return (
      <Fragment>
        <h1 className='page-title'>Editeur de Markdown</h1>
        <div className='container'>
          <div className='row'>
            <div className='col-sm-6'>
              <div dangerouslySetInnerHTML={this.renderText(this.state.text)}/>
            </div>
            <div className='col-sm-6'>
         <textarea
           onChange={this.handleChange}
           value={this.state.text}
           className='form-control'
           rows='33'/>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }

}

export default App
